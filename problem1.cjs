const fs = require('fs');
const path = require('path');
const randomFilesDirectory = path.join(__dirname, './randomFilesDirectory')



function createDirectory() {

  return new Promise((resolve, reject) =>

    fs.mkdir(randomFilesDirectory, { recursive: true }, (err) => {

      if (err) {
        reject(err)
      } else {
        console.log("Random Files Directory created")
        resolve()
      }
    })
  )
}


function createRandomFiles() {

  return new Promise((resolve, reject) => {

    fileCount = Math.floor(Math.random() * 10) + 1

    let fileNames = Array(fileCount).fill(0).map((element, index) => {
      return `./file-${index + element + 1}.json`
    })

    let checkList = fileNames.map((fileName, index) => {
      return createFile(fileName, `I am json file-${index + 1}`)
    })

    Promise.all(checkList)
      .then(() => {
        resolve(fileNames)
        console.log("All Files are created successfully")
      })
      .catch(err => {
        reject(err)
      })
  })
}


function createFile(fileName, data) {

  return new Promise((resolve, reject) => {

    fs.writeFile(path.join(randomFilesDirectory, fileName), JSON.stringify(data), (err) => {

      if (err) {
        reject(err)
      } else {
        console.log(`${fileName} is created`)
        resolve()
      }
    })
  })
}


function deleteRandomFiles(fileNames) {

  return new Promise(function (resolve, reject) {

    let checkList = fileNames.map((fileName) => {
      return deleteFile(fileName)
    })

    Promise.all(checkList)
      .then(() => {
        resolve()
        console.log("All Files are deleted successfully")
      })
      .catch(err => {
        reject(err)
      })
  })
}


function deleteFile(fileName) {

  return new Promise((resolve, reject) => {

    fs.unlink(path.join(randomFilesDirectory, fileName), (err) => {

      if (err) {
        reject(err)
      } else {
        console.log(`${fileName} is deleted`)
        resolve()
      }
    })
  })
}


function deleteDirectory() {

  return new Promise(function (resolve, reject) {

    fs.rmdir(randomFilesDirectory, (err) => {

      if (err) {
        reject(err)
      } else {
        console.log("Random Files Directory deleted")
        resolve()
      }
    })
  })
}



function problem1() {               // the main function starts here and then the thread begins from top

  return createDirectory()

    .then(createRandomFiles)
    .then(deleteRandomFiles)
    .then(deleteDirectory)
}


module.exports = problem1;