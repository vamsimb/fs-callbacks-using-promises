const fs = require('fs');
const path = require('path')
const filenamesPath = path.join(__dirname, './filenames.txt')
const lipsumPath = path.join(__dirname, './lipsum.txt')
const uppercasePath = path.join(__dirname, './upperCase.txt')
const splitToSentencesPath = path.join(__dirname, './splitToSentences.txt')
const sortSentencesPath = path.join(__dirname, './sortedSentences.txt')



function readFile(filePath) {

  return new Promise((resolve, reject) => {

    fs.readFile(filePath, "utf-8", (err, data) => {

      if (err) {
        reject(err);
      } else {
        console.log(`Reading successfull`)
        resolve(data)
      }
    })
  })
}


function writeFile(filePath, data) {

  return new Promise((resolve, reject) => {

    fs.writeFile(filePath, data, (err) => {

      if (err) {
        reject(err)
      } else {
        console.log(`Data successfully stored at ${filePath}`)
        resolve(filePath)
      }
    })
  })
}


function appendFile(fileName) {

  return new Promise((resolve, reject) => {

    fs.appendFile(filenamesPath, fileName + "+", (err) => {

      if (err) {
        reject(err)
      } else {
        console.log(`file name stored in filenames.txt`)
        resolve(fileName)
      }
    })
  })
}


function unlink(filePath) {

  return new Promise((resolve, reject) => {

    fs.unlink(filePath, (err) => {

      if (err) {
        reject(err)
      } else {
        console.log(`File successfully deleted`)
        resolve()
      }
    })
  })
}



function problem2() {              // the main function starts from here
  return readFile(lipsumPath)      // 1. Read the given file lipsum.txt

    .then(data => {
      let covertToUppercase = data.toUpperCase()
      return writeFile(uppercasePath, covertToUppercase)
    })

    .then(uppercaseFileName => {
      return appendFile(uppercaseFileName)
    })

    .then(uppercaseFilePath => {
      console.log('Task 2 completed successfully')
      return readFile(uppercaseFilePath)
    })

    .then(data => {
      let splitData = data.toLowerCase().split(". ").join("\n") // converted data splited into sentences}
      return writeFile(splitToSentencesPath, splitData)
    })

    .then(splitToSentencesFileName => {
      return appendFile(splitToSentencesFileName)
    })

    .then(splitToSentencesFilePath => {
      console.log('Task 3 completed successfully')
      return readFile(splitToSentencesFilePath)
    })

    .then(data => {
      let sortData = data.split("\n").sort().join('\n') // sort and joins the data
      return writeFile(sortSentencesPath, sortData)
    })

    .then(sortSentencesFileName => {
      return appendFile(sortSentencesFileName)
    })

    .then(() => {
      console.log('Task 4 completed successfully')
      return readFile(filenamesPath)
    })

    .then((data) => {
      console.log('Deleting all files')

      let fileNames = data.split("+")
      fileNames.pop()

      let checkList = fileNames.map(fileName => {
        return unlink(fileName)
      })

      Promise.all(checkList)              // this only exicates after all files are deleted.
        .then(() => {
          console.log('Deleting file Names file')
          return unlink(filenamesPath)
        })
        .catch((err) => {
          console.error(err)
        })
    })
}




module.exports = problem2;